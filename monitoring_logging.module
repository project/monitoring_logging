<?php

/**
 * @file
 * Monitoring Logging module file.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\monitoring_logging\Entity\LoggingConfig;

/**
 * Implements hook_monitoring_run_sensors().
 */
function monitoring_logging_monitoring_run_sensors(array $results) {
  if (Drupal::config('monitoring.settings')->get('sensor_call_logging') === 'none') {
    return;
  }

  foreach (LoggingConfig::loadMultiple() as $loggingConfig) {
    $loggingConfig->getLogger()->logResults(...array_values($results));
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function monitoring_logging_form_monitoring_settings_alter(&$form, FormStateInterface $form_state) {
  // Show warning if there is no logging.
  if (Drupal::config('monitoring.settings')->get('sensor_call_logging') == 'none') {
    // If logging is disabled, we don't save logs.
    $form['message'] = [
      '#type' => 'container',
      '#markup' => t('With logging disabled, monitoring logs are not written.'),
      '#attributes' => [
        'class' => ['messages messages--warning'],
      ],
    ];
  }
}
