<?php

namespace Drupal\Tests\monitoring_logging\Unit\Plugin\FileLogFormatter;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring\SensorConfigInterface;
use Drupal\monitoring_logging\Plugin\FileLogFormatter\FileLogFormatterDefault;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\monitoring_logging\Plugin\FileLogFormatter\FileLogFormatterDefault
 *
 * @group Monitoring Logging
 */
final class FileLogFormatterDefaultTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Test categories.
   *
   * @var string[]
   */
  protected $categories;

  /**
   * Test messages.
   *
   * @var string[]
   */
  protected $messages;

  /**
   * Test labels.
   *
   * @var string[]
   */
  protected $labels;

  /**
   * Test statuses.
   *
   * @var string[]
   */
  protected $statuses;

  /**
   * Mocked sensorconfigs.
   *
   * @var \Drupal\monitoring\SensorConfigInterface[]|\Prophecy\Prophecy\ProphecyInterface[]
   */
  protected $sensorConfigs;

  /**
   * Mocked results.
   *
   * @var \Drupal\monitoring\Result\SensorResultInterface[]|\Prophecy\Prophecy\ProphecyInterface[]
   */
  protected $results;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->categories = [
      uniqid(),
      uniqid(),
      uniqid(),
      uniqid(),
      uniqid(),
    ];

    $this->messages = [
      uniqid(),
      uniqid(),
      uniqid(),
      uniqid(),
      uniqid(),
    ];

    $this->labels = [
      uniqid(),
      uniqid(),
      uniqid(),
      uniqid(),
      uniqid(),
    ];

    $this->statuses = [
      SensorResultInterface::STATUS_CRITICAL,
      SensorResultInterface::STATUS_WARNING,
      SensorResultInterface::STATUS_OK,
      SensorResultInterface::STATUS_INFO,
      SensorResultInterface::STATUS_UNKNOWN,
    ];

    $this->sensorConfigs = [
      $this->prophesize(SensorConfigInterface::class),
      $this->prophesize(SensorConfigInterface::class),
      $this->prophesize(SensorConfigInterface::class),
      $this->prophesize(SensorConfigInterface::class),
      $this->prophesize(SensorConfigInterface::class),
    ];

    foreach ($this->sensorConfigs as $key => $config) {
      $config->getLabel()->willReturn($this->labels[$key]);
      $config->getCategory()->willReturn($this->categories[$key]);
    }

    $this->results = [
      $this->prophesize(SensorResultInterface::class),
      $this->prophesize(SensorResultInterface::class),
      $this->prophesize(SensorResultInterface::class),
      $this->prophesize(SensorResultInterface::class),
      $this->prophesize(SensorResultInterface::class),
    ];

    foreach ($this->results as $key => $result) {
      $result->getMessage()->willReturn($this->messages[$key]);
      $result->getStatus()->willReturn($this->statuses[$key]);
      $result->getSensorConfig()->willReturn($this->sensorConfigs[$key]);
    }
  }

  /**
   * Tests the format() method.
   *
   * @covers ::format
   */
  public function testFormat() {
    $formatter = $this->getFormatter([]);
    $results = [];
    foreach ($this->results as $result) {
      $results[] = $result->reveal();
    }
    $output = $formatter->format(...$results);
    foreach ($this->categories as $key => $category) {
      $this->assertStringContainsString('==== ' . strtoupper($category) . ' ====', $output);
      $this->assertStringContainsString('[' . $this->statuses[$key] . '] ' . $this->labels[$key] . ': ' . $this->messages[$key], $output);
    }
  }

  /**
   * Gets a formatter to test.
   *
   * @param array $configuration
   *   The formatter configuration.
   *
   * @return \Drupal\monitoring_logging\Plugin\FileLogFormatter\FileLogFormatterDefault
   *   The formatter to test.
   */
  protected function getFormatter(array $configuration) {
    return new FileLogFormatterDefault(
      $configuration,
      'default',
      [
        'provider' => 'monitoring_logging',
      ]
    );
  }

}
