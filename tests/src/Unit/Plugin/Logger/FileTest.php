<?php

namespace Drupal\Tests\monitoring_logging\Unit\Plugin\Logger;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring_logging\FileLogFormatterInterface;
use Drupal\monitoring_logging\FileLogFormatterManager;
use Drupal\monitoring_logging\Form\LoggerFileForm;
use Drupal\monitoring_logging\Plugin\Logger\File;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @coversDefaultClass \Drupal\monitoring_logging\Plugin\Logger\File
 *
 * @group Monitoring Logging
 */
final class FileTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Mocked formatter.
   *
   * @var \Drupal\monitoring_logging\FileLogFormatterInterface|\Prophecy\Prophecy\ProphecyInterface
   */
  protected $formatter;

  /**
   * Mocked file log formatter manager.
   *
   * @var \Drupal\monitoring_logging\FileLogFormatterManager|\Prophecy\Prophecy\ProphecyInterface
   */
  protected $fileLogFormatterManager;

  /**
   * Mocked filesystem.
   *
   * @var \Drupal\Core\File\FileSystemInterface|\Prophecy\Prophecy\ProphecyInterface
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->fileLogFormatterManager = $this->prophesize(FileLogFormatterManager::class);
    $this->fileSystem = $this->prophesize(FileSystemInterface::class);
    $this->formatter = $this->prophesize(FileLogFormatterInterface::class);
    $this->formatterId = uniqid();
    $this->fileLogFormatterManager->createInstance($this->formatterId)->willReturn($this->formatter);
  }

  /**
   * Tests the logResults() method.
   *
   * @covers ::logResults
   */
  public function testLogResults() {
    $formattedText = uniqid();
    $destination = uniqid();
    $results = [$this->prophesize(SensorResultInterface::class)->reveal()];
    $this->formatter->format(...$results)->willReturn($formattedText);

    $this->fileSystem
      ->saveData($formattedText, $destination, FileSystemInterface::EXISTS_REPLACE)
      ->shouldBeCalled()
      ->willReturn($destination);
    $logger = $this->getLogger([
      'file' => $destination,
      'formatter' => $this->formatterId,
    ]);
    $this->assertEquals($destination, $logger->logResults(...$results));
  }

  /**
   * Tests the getFormClass() method.
   *
   * @covers ::getFormClass
   */
  public function testGetFormClass() {
    $logger = $this->getLogger([]);
    $this->assertEquals(LoggerFileForm::class, $logger->getFormClass('configure'));
    $this->assertNull($logger->getFormClass(uniqid()));
  }

  /**
   * Tests the hasFormClass() method.
   *
   * @covers ::hasFormClass
   */
  public function testHasFormClass() {
    $logger = $this->getLogger([]);
    $this->assertTrue($logger->hasFormClass('configure'));
    $this->assertFalse($logger->hasFormClass(uniqid()));
  }

  /**
   * Tests the defaultConfiguration() method.
   *
   * @covers ::defaultConfiguration
   */
  public function testDefaultConfiguration() {
    $logger = $this->getLogger([]);
    $this->assertEquals(
      [
        'formatter' => 'default',
        'file' => '/var/log/monitoring.log',
      ],
      $logger->defaultConfiguration()
    );
  }

  /**
   * Tests the getConfiguration() method.
   *
   * @covers ::getConfiguration
   */
  public function testGetConfiguration() {
    $logger = $this->getLogger([]);
    $this->assertEquals(
      $logger->defaultConfiguration(),
      $logger->getConfiguration()
    );

    $destination = uniqid();
    $formatterId = uniqid();

    $configuration = [
      'formatter' => $formatterId,
    ];
    $logger = $this->getLogger($configuration);
    $this->assertEquals(
      $configuration + $logger->defaultConfiguration(),
      $logger->getConfiguration()
    );

    $configuration = [
      'file' => $destination,
    ];
    $logger = $this->getLogger($configuration);
    $this->assertEquals(
      $configuration + $logger->defaultConfiguration(),
      $logger->getConfiguration()
    );

    $configuration = [
      'formatter' => $formatterId,
      'file' => $destination,
    ];
    $logger = $this->getLogger($configuration);
    $this->assertEquals(
      $configuration + $logger->defaultConfiguration(),
      $logger->getConfiguration()
    );
  }

  /**
   * Tests the setConfiguration() method.
   *
   * @covers ::setConfiguration
   */
  public function testSetConfiguration() {
    $logger = $this->getLogger([]);
    $this->assertEquals(
      $logger->defaultConfiguration(),
      $logger->getConfiguration()
    );

    $destination = uniqid();
    $formatterId = uniqid();

    $configuration = [
      'formatter' => $formatterId,
    ];
    $logger->setConfiguration($configuration);
    $this->assertEquals(
      $configuration + $logger->defaultConfiguration(),
      $logger->getConfiguration()
    );

    $configuration = [
      'file' => $destination,
    ];
    $logger->setConfiguration([]);
    $this->assertEquals(
      $logger->defaultConfiguration(),
      $logger->getConfiguration()
    );
    $logger->setConfiguration($configuration);
    $this->assertEquals(
      $configuration + $logger->defaultConfiguration(),
      $logger->getConfiguration()
    );

    $configuration = [
      'formatter' => $formatterId,
      'file' => $destination,
    ];
    $logger->setConfiguration([]);
    $this->assertEquals(
      $logger->defaultConfiguration(),
      $logger->getConfiguration()
    );
    $logger->setConfiguration($configuration);
    $this->assertEquals(
      $configuration + $logger->defaultConfiguration(),
      $logger->getConfiguration()
    );
  }

  /**
   * Tests the getFormatter() method.
   *
   * @covers ::getFormatter
   */
  public function testGetFormatter() {
    $logger = $this->getLogger([
      'formatter' => $this->formatterId,
    ]);
    $this->assertEquals($this->formatter->reveal(), $logger->getFormatter());
  }

  /**
   * Tests the getFile() method.
   *
   * @covers ::getFile
   */
  public function testGetFile() {
    $logger = $this->getLogger([]);
    $this->assertEquals('/var/log/monitoring.log', $logger->getFile());

    $file = uniqid();
    $logger->setConfiguration([
      'file' => $file,
    ]);
    $this->assertEquals($file, $logger->getFile());
  }

  /**
   * Tests the calculateDependencies() method.
   *
   * @covers ::calculateDependencies
   */
  public function testCalculateDependencies() {
    $provider = uniqid();

    $moduleHandler = $this->prophesize(ModuleHandlerInterface::class);
    $moduleHandler->moduleExists($provider)->willReturn(TRUE);

    $themeHandler = $this->prophesize(ThemeHandlerInterface::class);
    $themeHandler->themeExists($provider)->willReturn(FALSE);

    $container = $this->prophesize(ContainerInterface::class);
    $container->get('module_handler')->willReturn($moduleHandler);
    $container->get('theme_handler')->willReturn($themeHandler);
    \Drupal::setContainer($container->reveal());

    $this->formatter->getPluginDefinition()->willReturn(['provider' => $provider]);
    $configuation = [
      'formatter' => $this->formatterId,
    ];
    $logger = $this->getLogger($configuation);
    $this->assertEquals(['module' => [$provider]], $logger->calculateDependencies());
  }

  /**
   * Get the logger to test.
   *
   * @param array $configuration
   *   The logger configuration.
   *
   * @return \Drupal\monitoring_logging\Plugin\Logger\File
   *   The logger to test.
   */
  protected function getLogger(array $configuration) {
    $logger = new File(
      $configuration,
      'file',
      [
        'provider' => 'monitoring_logging',
      ],
      $this->fileLogFormatterManager->reveal(),
      $this->fileSystem->reveal()
    );

    return $logger;
  }

}
