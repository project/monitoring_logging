<?php

namespace Drupal\monitoring_logging;

use Drupal\Component\Plugin\PluginInspectionInterface;;
use Drupal\monitoring\Result\SensorResultInterface;

/**
 * Interface for file log formatters.
 */
interface FileLogFormatterInterface extends PluginInspectionInterface {

  /**
   * Return the name of the file log formatter.
   *
   * @return string
   *   The plugin name.
   */
  public function getName();

  /**
   * Format the results in text.
   *
   * @param \Drupal\monitoring\Result\SensorResultInterface[] ...$results
   *   The results to format.
   *
   * @return string
   *   The formatted message.
   */
  public function format(SensorResultInterface ...$results);

}
