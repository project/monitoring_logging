<?php

namespace Drupal\monitoring_logging;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\monitoring\Result\SensorResultInterface;

/**
 * Logger interface.
 */
interface LoggerInterface extends PluginInspectionInterface {

  /**
   * Return the name of the logger.
   *
   * @return string
   *   The name of the logger.
   */
  public function getName();

  /**
   * Log the sensor results.
   *
   * @param \Drupal\monitoring\Result\SensorResultInterface ...$results
   *   The sensor results.
   *
   * @return string
   *   A string with the path of the resulting file, or FALSE on error.
   *
   * @throws \Drupal\Core\File\Exception\FileException
   *   Implementation may throw FileException or its subtype on failure.
   */
  public function logResults(SensorResultInterface ...$results);

}
