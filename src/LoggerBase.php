<?php

namespace Drupal\monitoring_logging;

use Drupal\Component\Plugin\PluginBase;

/**
 * Logger base class.
 */
abstract class LoggerBase extends PluginBase implements LoggerInterface {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->pluginDefinition['name'];
  }

}
