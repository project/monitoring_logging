<?php

namespace Drupal\monitoring_logging;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The file log formatter manager.
 */
class FileLogFormatterManager extends DefaultPluginManager {

  /**
   * Constructs a LoggerManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cacheBackend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/FileLogFormatter', $namespaces, $module_handler, 'Drupal\monitoring_logging\FileLogFormatterInterface', 'Drupal\monitoring_logging\Annotation\FileLogFormatter');
    $this->alterInfo('monitoring_logging_file_log_formatter_info');
    $this->setCacheBackend($cacheBackend, 'monitoring_logging_file_log_formatters');
  }

}
