<?php

namespace Drupal\monitoring_logging\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * File log formatter annotation.
 *
 * @Annotation
 */
class FileLogFormatter extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the formatter.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

}
