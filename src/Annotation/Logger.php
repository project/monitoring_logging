<?php

namespace Drupal\monitoring_logging\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Logger annotation.
 *
 * @Annotation
 */
class Logger extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the logger.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

}
