<?php

namespace Drupal\monitoring_logging\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\monitoring_logging\LoggerInterface;
use Drupal\monitoring_logging\LoggerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoggingConfigForm.
 */
class LoggingConfigForm extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\monitoring_logging\Entity\LoggingConfig
   */
  protected $entity;

  /**
   * The plugin form manager.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  /**
   * The monitoring logger plugin manager.
   *
   * @var \Drupal\monitoring_logging\LoggerManager
   */
  protected $loggerManager;

  /**
   * Form constructor.
   *
   * @param \Drupal\Core\Plugin\PluginFormFactoryInterface $pluginFormFactory
   *   The plugin form manager.
   * @param \Drupal\monitoring_logging\LoggerManager $loggerManager
   *   The monitoring logger plugin manager.
   */
  public function __construct(PluginFormFactoryInterface $pluginFormFactory, LoggerManager $loggerManager) {
    $this->pluginFormFactory = $pluginFormFactory;
    $this->loggerManager = $loggerManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin_form.factory'),
      $container->get('plugin.manager.monitoring_logger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $this->entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t("Label for the Monitoring logging config."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\monitoring_logging\Entity\LoggingConfig::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $logger = $this->entity->getLogger();
    $loggers = $this->loggerManager->getDefinitions();
    $options = [];
    foreach ($loggers as $id => $definition) {
      $options[$id] = $definition['name'];
    }

    $form['logger_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Logger'),
      '#options' => $options,
      '#default_value' => $logger ? $logger->getPluginId() : '',
      '#required' => TRUE,
      '#empty_value' => '',
      '#empty_option' => t('- None -'),
      '#ajax' => [
        'callback' => '::loggerSettingsFormAjax',
        'event' => 'change',
        'wrapper' => 'configuration-container',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Verifying entry...'),
        ],
      ],
    ];

    $form['configuration'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'configuration-container',
      ],
    ];

    if ($this->entity->getLogger() instanceof PluginWithFormsInterface) {
      $form['configuration'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t('Formatter'),
        '#tree' => TRUE,
        '#attributes' => [
          'id' => 'configuration-container',
        ],
      ];
      $subform_state = SubformState::createForSubform($form['configuration'], $form, $form_state);
      $form['configuration'] = $this->getPluginForm($this->entity->getLogger())->buildConfigurationForm($form['configuration'], $subform_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $logger = $this->entity->getLogger();
    if ($logger instanceof PluginWithFormsInterface) {
      $subform_state = SubformState::createForSubform($form['configuration'], $form, $form_state);
      $this->getPluginForm($logger)->validateConfigurationForm($form['configuration'], $subform_state);
    }
  }

  /**
   * Ajax callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The part of the form to replace.
   */
  public function loggerSettingsFormAjax(array &$form, FormStateInterface $form_state) {
    return $form['configuration'];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = $this->entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Monitoring logging config.', [
          '%label' => $this->entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Monitoring logging config.', [
          '%label' => $this->entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

  /**
   * Retrieves the plugin form for a given block and operation.
   *
   * @param \Drupal\monitoring_logging\LoggerInterface $logger
   *   The logger plugin.
   *
   * @return \Drupal\Core\Plugin\PluginFormInterface
   *   The plugin form for the logger.
   */
  protected function getPluginForm(LoggerInterface $logger) {
    if ($logger instanceof PluginWithFormsInterface) {
      return $this->pluginFormFactory->createInstance($logger, 'configure');
    }
    return FALSE;
  }

}
