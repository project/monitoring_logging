<?php

namespace Drupal\monitoring_logging\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\monitoring_logging\FileLogFormatterManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form class.
 */
class LoggerFileForm extends PluginFormBase implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The file log formatter plugin manager.
   *
   * @var \Drupal\monitoring_logging\FileLogFormatterManager
   */
  protected $fileLogFormatterManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;
  
  /**
   * The Drupal root directory.
   *
   * @var string
   */
  protected $drupalRoot;

  /**
   * Creates a config form for a file logger.
   *
   * @param \Drupal\monitoring_logging\FileLogFormatterManager $fileLogFormatterManager
   *   The file log formatter plugin manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   The translation service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system implementation.
   * @param string $root
   *   The Drupal root directory.
   */
  public function __construct(FileLogFormatterManager $fileLogFormatterManager, TranslationInterface $stringTranslation, FileSystemInterface $fileSystem, $root) {
    $this->fileLogFormatterManager = $fileLogFormatterManager;
    $this->stringTranslation = $stringTranslation;
    $this->fileSystem = $fileSystem;
    $this->drupalRoot = $root;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.monitoring_file_log_formatter'),
      $container->get('string_translation'),
      $container->get('file_system'),
      $container->getParameter('app.root')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $formatter = $this->plugin->getFormatter();
    $formatters = $this->fileLogFormatterManager->getDefinitions();
    $options = [];
    foreach ($formatters as $id => $definition) {
      $options[$id] = $definition['name'];
    }
    $form['formatter'] = [
      '#type' => 'select',
      '#title' => $this->t('Formatter'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $formatter ? $formatter->getPluginId() : '',
    ];
    $form['file'] = [
      '#type' => 'textfield',
      '#title' => t('Log file'),
      '#maxlength' => 255,
      '#default_value' => $this->plugin->getFile(),
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    if (!$form_state->hasValue('file')) {
      return;
    }
    $dir = $this->fileSystem->dirname($form_state->getValue('file'));
    try {
      if (!$this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
        $form_state->setError($form['file'], $this->t('The directory could not be created or is not writable.'));
        return;
      }
    }
    catch (FileException $e) {
      $form_state->setError($form['file'], $e->getMessage());
      return;
    }

    if (strpos($this->fileSystem->realpath($dir), $this->drupalRoot) === 0) {
      $form_state->setError($form['file'], $this->t("Please choose a location outside of the Drupal root directory. The log may contain sensitive data (like available security updates) that we don't want to expose to the world."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->plugin->setConfiguration(['formatter' => $form_state->get('formatter'), 'file' => $form_state->get('file')]);
  }

}
