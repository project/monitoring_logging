<?php

namespace Drupal\monitoring_logging\Plugin\Logger;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginDependencyTrait;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring_logging\FileLogFormatterManager;
use Drupal\monitoring_logging\Form\LoggerFileForm;
use Drupal\monitoring_logging\LoggerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * File logger.
 *
 * @Logger(
 *   id="file",
 *   name=@Translation("File")
 * )
 */
class File extends LoggerBase implements PluginWithFormsInterface, ConfigurableInterface, ContainerFactoryPluginInterface, DependentPluginInterface {

  use PluginDependencyTrait;

  /**
   * The file log formatter manager.
   *
   * @var \Drupal\monitoring_logging\FileLogFormatterManager
   */
  protected $fileLogFormatterManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Creates a file logger.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\monitoring_logging\FileLogFormatterManager $fileLogFormatterManager
   *   The file log formatter plugin manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system implementation.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    FileLogFormatterManager $fileLogFormatterManager,
    FileSystemInterface $fileSystem
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fileLogFormatterManager = $fileLogFormatterManager;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.monitoring_file_log_formatter'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function logResults(SensorResultInterface ...$results) {
    try {
      return $this->fileSystem->saveData($this->getFormatter()->format(...$results), $this->getFile(), FileSystemInterface::EXISTS_REPLACE);
    }
    catch (FileException $e) {
      watchdog_exception('monitoring_logging', $e);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormClass($operation) {
    return $operation === 'configure' ? LoggerFileForm::class : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function hasFormClass($operation) {
    return $operation === 'configure';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'formatter' => 'default',
      'file' => '/var/log/monitoring.log',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * Gets the formatter plugin to format the message.
   *
   * @return \Drupal\monitoring_logging\FileLogFormatterInterface|bool
   *   The formatter, false if no formatter is set, or the plugin is not found.
   */
  public function getFormatter() {
    if ($this->getConfiguration()['formatter']) {
      try {
        return $this->fileLogFormatterManager->createInstance($this->getConfiguration()['formatter']);
      }
      catch (PluginNotFoundException $ex) {
        watchdog_exception('monitoring_logging', $ex);
      }
    }
    return FALSE;
  }

  /**
   * Gets the file to log to.
   *
   * @return string|bool
   *   The file path, false if not set.
   */
  public function getFile() {
    if ($this->getConfiguration()['file']) {
      return $this->getConfiguration()['file'];
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    if ($formatter = $this->getFormatter()) {
      $this->calculatePluginDependencies($formatter);
    }
    return $this->dependencies;
  }

}
