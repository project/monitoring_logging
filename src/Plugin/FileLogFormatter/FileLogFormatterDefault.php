<?php

namespace Drupal\monitoring_logging\Plugin\FileLogFormatter;

use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring_logging\FileLogFormatterBase;

/**
 * Default file log formatter.
 *
 * @FileLogFormatter(
 *   id="default",
 *   name=@Translation("Default")
 * )
 */
class FileLogFormatterDefault extends FileLogFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function format(SensorResultInterface ...$results) {
    $groupedResults = $this->groupResults(...$results);
    $data = [];
    foreach ($groupedResults as $severity => $categoryResults) {
      foreach ($categoryResults as $category => $results) {
        $data[] = sprintf('==== %s ====', strtoupper($category));
        foreach ($results as $result) {
          $data[] = sprintf(
            '[%s] %s: %s',
            $severity,
            $result->getSensorConfig()->getLabel(),
            $result->getMessage()
          );
        }
      }
    }
    return implode(PHP_EOL, $data);
  }

  /**
   * Groups the results per severity, per category.
   *
   * @param \Drupal\monitoring\Result\SensorResultInterface ...$results
   *   The sensor results.
   *
   * @return array
   *   The grouped results.
   */
  protected function groupResults(SensorResultInterface ...$results) {
    $groupedResults = [];
    foreach ($results as $result) {
      $groupedResults[$result->getStatus()][$result->getSensorConfig()->getCategory()][] = $result;
    }

    return $groupedResults;
  }

}
