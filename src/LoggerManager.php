<?php

namespace Drupal\monitoring_logging;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Logger manager.
 */
class LoggerManager extends DefaultPluginManager {

  /**
   * Constructs a LoggerManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cacheBackend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Logger', $namespaces, $module_handler, 'Drupal\monitoring_logging\LoggerInterface', 'Drupal\monitoring_logging\Annotation\Logger');
    $this->alterInfo('monitoring_logging_logger_info');
    $this->setCacheBackend($cacheBackend, 'monitoring_logging_logger_loggers');
  }

}
