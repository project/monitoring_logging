<?php

namespace Drupal\monitoring_logging;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for file log formatters.
 */
abstract class FileLogFormatterBase extends PluginBase implements FileLogFormatterInterface {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->pluginDefinition['name'];
  }

}
