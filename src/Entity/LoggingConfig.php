<?php

namespace Drupal\monitoring_logging\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;

/**
 * Defines the Monitoring logging config entity.
 *
 * @ConfigEntityType(
 *   id = "monitoring_logging_config",
 *   label = @Translation("Monitoring logging config"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\monitoring_logging\LoggingConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\monitoring_logging\Form\LoggingConfigForm",
 *       "edit" = "Drupal\monitoring_logging\Form\LoggingConfigForm",
 *       "delete" = "Drupal\monitoring_logging\Form\LoggingConfigDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\monitoring_logging\LoggingConfigHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "monitoring_logging_config",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/system/monitoring/logging/{monitoring_logging_config}",
 *     "add-form" = "/admin/config/system/monitoring/logging/add",
 *     "edit-form" = "/admin/config/system/monitoring/logging/{monitoring_logging_config}/edit",
 *     "delete-form" = "/admin/config/system/monitoring/logging/{monitoring_logging_config}/delete",
 *     "collection" = "/admin/config/system/monitoring/logging"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "logger_id",
 *     "configuration",
 *   }
 * )
 */
class LoggingConfig extends ConfigEntityBase implements LoggingConfigInterface {

  /**
   * The Monitoring logging config ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Monitoring logging config label.
   *
   * @var string
   */
  protected $label;

  /**
   * The logger plugin id.
   *
   * @var string
   */
  protected $logger_id;

  /**
   * The logger configuration.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    if ($this->logger_id) {
      return [
        'logger' => new DefaultSingleLazyPluginCollection(\Drupal::service('plugin.manager.monitoring_logger'), $this->logger_id, $this->configuration ?? []),
      ];
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLogger() {
    $collection = $this->getPluginCollections();
    if (isset($collection['logger']) && $this->logger_id) {
      return $this->getPluginCollections()['logger']->get($this->logger_id);
    }
    return FALSE;
  }

}
