<?php

namespace Drupal\monitoring_logging\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Provides an interface for defining Monitoring logging config entities.
 */
interface LoggingConfigInterface extends ConfigEntityInterface, EntityWithPluginCollectionInterface {

  /**
   * Get the logger plugin instance.
   *
   * @return \Drupal\monitoring_logging\LoggerInterface
   *   The logger plugin instance.
   */
  public function getLogger();

}
