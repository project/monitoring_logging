<?php

namespace Drupal\monitoring_logging_check_drupal\Plugin\FileLogFormatter;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\monitoring\Entity\SensorResultDataInterface;
use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring_logging\FileLogFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Nagios formatter for the file logger.
 *
 * @FileLogFormatter(
 *   id="nagios",
 *   name=@Translation("Nagios")
 * )
 */
class FileLogFormatterNagios extends FileLogFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Exit code constants.
   */
  const EXIT_OK = 0;
  const EXIT_WARN = 1;
  const EXIT_ERR = 2;
  const EXIT_UNKNOWN = 3;

  /**
   * Map for mapping statuses to exit codes.
   */
  const STATUS_MAPPING = [
    SensorResultDataInterface::STATUS_OK => self::EXIT_OK,
    SensorResultDataInterface::STATUS_INFO => self::EXIT_OK,
    SensorResultDataInterface::STATUS_WARNING => self::EXIT_WARN,
    SensorResultDataInterface::STATUS_CRITICAL => self::EXIT_ERR,
    SensorResultDataInterface::STATUS_UNKNOWN => self::EXIT_UNKNOWN,
  ];

  /**
   * Map for mapping exit codes to labels.
   */
  const EXIT_LABELS = [
    self::EXIT_OK => 'OK',
    self::EXIT_WARN => 'WARNING',
    self::EXIT_ERR => 'CRITICAL',
    self::EXIT_UNKNOWN => 'UNKNOWN',
  ];

  /**
   * The name of the site associated with the formatter.
   * This property holds the name of the site for which the formatter is being used.
   * It is typically set during the instantiation of the formatter.
   *
   * @var string
   *   The name of the site.
   */
  protected $siteName;

  /**
   * Constructs a nagios formatter plugin instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param string $siteName
   *   The site name.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $siteName) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->siteName = $siteName;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('system.site')->get('name')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function format(SensorResultInterface ...$results) {
    $groupedResults = $this->groupResults(...$results);
    return implode(
      PHP_EOL,
      [
        $this->header($groupedResults),
        $this->extendedData($groupedResults),
        $this->exitCode($groupedResults),
      ]
    ) . PHP_EOL;
  }

  /**
   * Groups the results per severity, per category.
   *
   * @param \Drupal\monitoring\Result\SensorResultInterface ...$results
   *   The sensor results.
   *
   * @return array
   *   The grouped results.
   */
  public function groupResults(SensorResultInterface ...$results) {
    $groupedResults = [
      static::EXIT_ERR => [],
      static::EXIT_WARN => [],
      static::EXIT_UNKNOWN => [],
      static::EXIT_OK => [],
    ];
    foreach ($results as $result) {
      $groupedResults[static::STATUS_MAPPING[$result->getStatus()]][$result->getSensorConfig()->getCategory()][] = $result;
    }

    return $groupedResults;
  }

  /**
   * Format the header.
   *
   * @param \Drupal\monitoring\Result\SensorResultInterface[] $groupedResults
   *   The grouped results.
   *
   * @return string
   *   The formatted header.
   */
  protected function header(array $groupedResults) {
    $header = '';
    $exitCode = $this->exitCode($groupedResults);
    switch ($exitCode) {
      case static::EXIT_OK:
        $header = sprintf('[%s] %s is healthy', static::EXIT_LABELS[$exitCode], $this->siteName);
        break;

      case static::EXIT_WARN:
        $header = sprintf('[%s] %s has warnings: %s', static::EXIT_LABELS[$exitCode], $this->siteName, $this->summary($groupedResults));
        break;

      case static::EXIT_ERR:
        $header = sprintf('[%s] %s has errors: %s', static::EXIT_LABELS[$exitCode], $this->siteName, $this->summary($groupedResults));
        break;

      case static::EXIT_UNKNOWN:
        $header = sprintf('[%s] %s is at unknown state - check extended info', static::EXIT_LABELS[$exitCode], $this->siteName);
        break;

    }

    return $header . " | " . $this->perfData($groupedResults);
  }

  /**
   * Format a summary for the header.
   *
   * @param \Drupal\monitoring\Result\SensorResultInterface[] $groupedResults
   *   The grouped results.
   *
   * @return string
   *   The formatted summary.
   */
  protected function summary(array $groupedResults) {
    $summary = [];
    $counts = array_filter(array_map(function($statusResults) {
      return array_sum(array_map('count', $statusResults));
    }, $groupedResults));
    krsort($counts);
    foreach ($counts as $severity => $count) {
      switch ($severity) {
        case static::EXIT_WARN:
          $summary[] = sprintf('%d %s', $count, $count === 1 ? 'Warning' : 'Warnings');
          break;

        case static::EXIT_ERR:
          $summary[] = sprintf('%d %s', $count, $count === 1 ? 'Error' : 'Errors');
          break;

        case static::EXIT_UNKNOWN:
          $summary[] = sprintf('%d %s', $count, $count === 1 ? 'Unknown' : 'Unknowns');
          break;

      }
    }

    return implode(', ', $summary);
  }

  /**
   * Format the perfdata.
   *
   * @param \Drupal\monitoring\Result\SensorResultInterface[] $groupedResults
   *   The grouped results.
   *
   * @return string
   *   The formatted perfdata.
   *
   * @see https://icinga.com/docs/icinga1/latest/en/perfdata.html#formatperfdata
   */
  protected function perfData(array $groupedResults) {
    $perfData = [];
    $counts = array_map(function($statusResults) {
      return array_sum(array_map('count', $statusResults));
    }, $groupedResults);
    $total = array_sum($counts);
    foreach ($counts as $severity => $count) {
      switch ($severity) {
        case static::EXIT_OK:
          $perfData[] = sprintf("'OK'=%d;;;0;%d", $count, $total);
          break;

        case static::EXIT_WARN:
          $perfData[] = sprintf("'Warnings'=%d;1;;0;%d", $count, $total);
          break;

        case static::EXIT_ERR:
          $perfData[] = sprintf("'Errors'=%d;1;1;0;%d", $count, $total);
          break;

        case static::EXIT_UNKNOWN:
          $perfData[] = sprintf("'Unknown'=%d;;;0;%d", $count, $total);
          break;

      }
    }

    return implode(' ', $perfData);
  }

  /**
   * Format the main (human readable) part of the message.
   *
   * @param \Drupal\monitoring\Result\SensorResultInterface[] $groupedResults
   *   The grouped results.
   *
   * @return string
   *   The formatted message.
   */
  protected function extendedData(array $groupedResults) {
    $extendedData = [];
    foreach ($groupedResults as $severity => $categoryResults) {
      foreach ($categoryResults as $category => $results) {
        $extendedData[] = sprintf('==== %s ====', strtoupper($category));
        foreach ($results as $result) {
          $extendedData[] = sprintf(
            '[%s] %s: %s',
            static::EXIT_LABELS[$severity],
            $result->getSensorConfig()->getLabel(),
            $result->getMessage()
          );
        }
      }
    }

    return implode(PHP_EOL, $extendedData);
  }

  /**
   * Format the exit code.
   *
   * @param \Drupal\monitoring\Result\SensorResultInterface[] $groupedResults
   *   The grouped results.
   *
   * @return string
   *   The formatted exit code.
   */
  protected function exitCode(array $groupedResults) {
    $counts = array_map(function($statusResults) {
      return array_sum(array_map('count', $statusResults));
    }, $groupedResults);
    $orderedExits = [
      static::EXIT_ERR,
      static::EXIT_WARN,
      static::EXIT_UNKNOWN,
      static::EXIT_OK,
    ];
    foreach ($orderedExits as $exit) {
      if ($counts[$exit] > 0) {
        return $exit;
      }
    }

    return static::EXIT_UNKNOWN;
  }

}
