# Monitoring Logging

This module allows you to log sensor results.

This module provides a framework for writing plugins that log the results to
different systems. There is an example implementation that logs the results to a
file, using a formatter plugin.

There is also a submodule providing a formatter plugin, that formats the logfile
so that it can be read by the Drupal nagios plugin.
